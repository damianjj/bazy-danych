﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProjektBazyDanych
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();

            /*Employee e1 = new Employee("admin", "admin", "admin");
            Employee e2 = new Employee("pracownik1", "pracownik1", "Pracownik Jeden");
            Employee e3 = new Employee("pracownik2", "pracownik2", "Pracownik Dwa");
            ServiceName sn1 = new ServiceName("zabieg1");
            ServiceName sn2 = new ServiceName("zabieg2");
            ServiceName sn3 = new ServiceName("zabieg3");
            ServiceName sn4 = new ServiceName("zabieg4");
            ServiceName sn5 = new ServiceName("zabieg5");
            ServiceName sn6 = new ServiceName("zabieg6");

            using (var ctx = new ClinicDbContext())
            {
                ctx.Employees.Add(e1);
                ctx.Employees.Add(e2);
                ctx.Employees.Add(e3);
                ctx.ServiceNames.Add(sn1);
                ctx.ServiceNames.Add(sn2);
                ctx.ServiceNames.Add(sn3);
                ctx.ServiceNames.Add(sn4);
                ctx.ServiceNames.Add(sn5);
                ctx.ServiceNames.Add(sn6);
                ctx.SaveChanges();
            }*/
        }     

        private void LogButton_Click(object sender, EventArgs e)
        {
            if (loginTextBox.Text == "" | passwdTextBox.Text == "")
            {
                MessageBox.Show("Wypełnij wszystkie pola!");
                return;
            }

            Patient log = new Patient();

            if (log.LogCheck(loginTextBox.Text, passwdTextBox.Text))
            {
                PatientForm f = new PatientForm(loginTextBox.Text);
                this.Hide();
                f.Show();
            }
            else MessageBox.Show("Błędny login lub hasło!");
        }

        private void FormClose(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        private void RegisterLabel_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            RegisterForm f = new RegisterForm();
            this.Hide();
            f.Show();
        }

        private void EmployeeLabel_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            EmployeeLogForm f = new EmployeeLogForm();
            this.Hide();
            f.Show();
        }

        private void ShowLabel_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            SeeForm f = new SeeForm();
            this.Hide();
            f.Show();
        }
    }
}

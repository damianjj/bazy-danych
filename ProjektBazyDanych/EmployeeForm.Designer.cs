﻿namespace ProjektBazyDanych
{
    partial class EmployeeForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelEmployeeForm = new System.Windows.Forms.Label();
            this.buttonAddPatient = new System.Windows.Forms.Button();
            this.buttonSearchPatient = new System.Windows.Forms.Button();
            this.showAllButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // labelEmployeeForm
            // 
            this.labelEmployeeForm.AutoSize = true;
            this.labelEmployeeForm.Location = new System.Drawing.Point(83, 24);
            this.labelEmployeeForm.Name = "labelEmployeeForm";
            this.labelEmployeeForm.Size = new System.Drawing.Size(141, 13);
            this.labelEmployeeForm.TabIndex = 0;
            this.labelEmployeeForm.Text = "Zalogowano jako pracownik";
            // 
            // buttonAddPatient
            // 
            this.buttonAddPatient.Location = new System.Drawing.Point(86, 62);
            this.buttonAddPatient.Name = "buttonAddPatient";
            this.buttonAddPatient.Size = new System.Drawing.Size(122, 23);
            this.buttonAddPatient.TabIndex = 1;
            this.buttonAddPatient.Text = "Umów wizytę";
            this.buttonAddPatient.UseVisualStyleBackColor = true;
            this.buttonAddPatient.Click += new System.EventHandler(this.ButtonAddPatient_Click);
            // 
            // buttonSearchPatient
            // 
            this.buttonSearchPatient.Location = new System.Drawing.Point(86, 131);
            this.buttonSearchPatient.Name = "buttonSearchPatient";
            this.buttonSearchPatient.Size = new System.Drawing.Size(122, 23);
            this.buttonSearchPatient.TabIndex = 3;
            this.buttonSearchPatient.Text = "Wyszukaj pacjenta";
            this.buttonSearchPatient.UseVisualStyleBackColor = true;
            this.buttonSearchPatient.Click += new System.EventHandler(this.ButtonSearchPatient_Click);
            // 
            // showAllButton
            // 
            this.showAllButton.Location = new System.Drawing.Point(86, 198);
            this.showAllButton.Name = "showAllButton";
            this.showAllButton.Size = new System.Drawing.Size(122, 42);
            this.showAllButton.TabIndex = 4;
            this.showAllButton.Text = "Wyświetl wszystkie wizyty";
            this.showAllButton.UseVisualStyleBackColor = true;
            this.showAllButton.Click += new System.EventHandler(this.ShowAllButton_Click);
            // 
            // EmployeeForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(286, 252);
            this.Controls.Add(this.showAllButton);
            this.Controls.Add(this.buttonSearchPatient);
            this.Controls.Add(this.buttonAddPatient);
            this.Controls.Add(this.labelEmployeeForm);
            this.Name = "EmployeeForm";
            this.Text = "Panel pracownika";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormClose);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelEmployeeForm;
        private System.Windows.Forms.Button buttonAddPatient;
        private System.Windows.Forms.Button buttonSearchPatient;
        private System.Windows.Forms.Button showAllButton;
    }
}
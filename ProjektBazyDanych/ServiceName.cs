﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjektBazyDanych
{
    public class ServiceName
    {
        public int ServiceNameID { get; set; }
        public string Name { get; set; }

        public ICollection<Service> Services { get; set; }

        public ServiceName() { }

        public ServiceName(string name)
        {
            this.Name = name;
        }
    }
}

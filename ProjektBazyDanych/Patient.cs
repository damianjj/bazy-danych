﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProjektBazyDanych
{
    public class Patient
    {
        public int PatientID { get; set; }
        public string Login { get; set; }
        public string Passwd { get; set; }
        public string Name { get; set; }
        public int PESEL { get; set; }

        public ICollection<Service> Services { get; set; }

        public virtual Insurance Insurance { get; set; }

        public Patient() { }

        public Patient(string login, string passwd, string name, int pesel, Insurance insurance)
        {
            this.Login = login;
            this.Passwd = passwd;
            this.Name = name;
            this.PESEL = pesel;
            this.Insurance = insurance;
        }

        public bool LogCheck(string log, string passwd)
        {
            using (var ctx = new ClinicDbContext())
            {
                foreach (var q in ctx.Patients)
                {
                    if (log == q.Login & passwd == q.Passwd)
                    {
                        return true;
                    }
                }
            }
            return false;
        }
    }
}

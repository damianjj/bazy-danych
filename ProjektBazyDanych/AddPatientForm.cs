﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProjektBazyDanych
{
    public partial class AddPatientForm : Form
    {
        public AddPatientForm(string who)
        {
            InitializeComponent();
            Set(who);

            if (who != "admin")
            {
                employeeNameTextBox.Text = who;
                employeeNameTextBox.Enabled = false;
            }
        }

        string who1;
        bool error = true;

        private void Set(string who)
        {
            who1 = who;
        }

        private void ButtonAdd_Click(object sender, EventArgs e)
        {
            if (loginTextBox.Text == "" | serviceTextBox.Text == "" | dateTextBox.Text == "" | costTextBox.Text == "" | employeeNameTextBox.Text == "")
            {
                MessageBox.Show("Wypełnij wszystkie pola!");
                return;
            }

            try
            {
                using (var ctx = new ClinicDbContext())
                {
                    foreach (var q in ctx.Services)
                    {
                        if (q.Employee.Login == employeeNameTextBox.Text & q.Duration == DateTime.Parse(dateTextBox.Text))
                        {
                            MessageBox.Show("Nie można umówić wizyty!\nPodany lekarz nie jest wolny w tym terminie!");
                            return;
                        }
                    }

                    foreach (var q in ctx.Patients)
                    {
                        if (who1 == "admin")
                        {
                            Employee em = new Employee();

                            foreach (var w in ctx.Employees)
                            {
                                if (w.Login == employeeNameTextBox.Text)
                                {
                                    if (loginTextBox.Text == q.Login)
                                    {
                                        foreach (var t in ctx.ServiceNames)
                                        {
                                            if (serviceTextBox.Text == t.Name)
                                            {
                                                Service service = new Service(t, DateTime.Parse(dateTextBox.Text), int.Parse(costTextBox.Text), q, w);
                                                ctx.Services.Add(service);
                                                error = false;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            foreach (var w in ctx.Employees)
                            {
                                if (who1 == w.Login)
                                {
                                    if (loginTextBox.Text == q.Login)
                                    {
                                        foreach (var t in ctx.ServiceNames)
                                        {
                                            if (serviceTextBox.Text == t.Name)
                                            {
                                                Service service = new Service(t, DateTime.Parse(dateTextBox.Text), int.Parse(costTextBox.Text), q, w);
                                                ctx.Services.Add(service);
                                                error = false;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    if (error)
                    {
                        MessageBox.Show("Nie ma takiego zabiegu!\nWpisz prawidłową nazwę zabiegu!");
                        return;
                    }
                    ctx.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Błąd!\nWprowadzono nieodpowiednie dane!");
            }
            
            this.Close();
        }
    }
}

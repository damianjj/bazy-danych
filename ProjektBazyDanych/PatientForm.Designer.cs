﻿namespace ProjektBazyDanych
{
    partial class PatientForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelPatientForm = new System.Windows.Forms.Label();
            this.patientRichBox = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // labelPatientForm
            // 
            this.labelPatientForm.AutoSize = true;
            this.labelPatientForm.Location = new System.Drawing.Point(82, 26);
            this.labelPatientForm.Name = "labelPatientForm";
            this.labelPatientForm.Size = new System.Drawing.Size(127, 13);
            this.labelPatientForm.TabIndex = 0;
            this.labelPatientForm.Text = "Zalogowano jako pacjent";
            // 
            // patientRichBox
            // 
            this.patientRichBox.Enabled = false;
            this.patientRichBox.Location = new System.Drawing.Point(47, 56);
            this.patientRichBox.Name = "patientRichBox";
            this.patientRichBox.Size = new System.Drawing.Size(198, 193);
            this.patientRichBox.TabIndex = 1;
            this.patientRichBox.Text = "";
            // 
            // PatientForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.patientRichBox);
            this.Controls.Add(this.labelPatientForm);
            this.Name = "PatientForm";
            this.Text = "Panel pacjenta";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormClose);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelPatientForm;
        private System.Windows.Forms.RichTextBox patientRichBox;
    }
}
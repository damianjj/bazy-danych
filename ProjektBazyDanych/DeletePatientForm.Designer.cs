﻿namespace ProjektBazyDanych
{
    partial class DeletePatientForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxPatientNameDelete = new System.Windows.Forms.TextBox();
            this.labelPatientNameDelete = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.buttonPatientDelete = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // textBoxPatientNameDelete
            // 
            this.textBoxPatientNameDelete.Location = new System.Drawing.Point(97, 54);
            this.textBoxPatientNameDelete.Name = "textBoxPatientNameDelete";
            this.textBoxPatientNameDelete.Size = new System.Drawing.Size(147, 20);
            this.textBoxPatientNameDelete.TabIndex = 0;
            // 
            // labelPatientNameDelete
            // 
            this.labelPatientNameDelete.AutoSize = true;
            this.labelPatientNameDelete.Location = new System.Drawing.Point(14, 57);
            this.labelPatientNameDelete.Name = "labelPatientNameDelete";
            this.labelPatientNameDelete.Size = new System.Drawing.Size(77, 13);
            this.labelPatientNameDelete.TabIndex = 4;
            this.labelPatientNameDelete.Text = "Login pacjenta";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(105, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "Dane pacjenta";
            // 
            // buttonPatientDelete
            // 
            this.buttonPatientDelete.Location = new System.Drawing.Point(85, 92);
            this.buttonPatientDelete.Name = "buttonPatientDelete";
            this.buttonPatientDelete.Size = new System.Drawing.Size(135, 53);
            this.buttonPatientDelete.TabIndex = 8;
            this.buttonPatientDelete.Text = "Znajdź i usuń pacjenta";
            this.buttonPatientDelete.UseVisualStyleBackColor = true;
            this.buttonPatientDelete.Click += new System.EventHandler(this.ButtonPatientDelete_Click);
            // 
            // DeletePatientForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(306, 181);
            this.Controls.Add(this.buttonPatientDelete);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.labelPatientNameDelete);
            this.Controls.Add(this.textBoxPatientNameDelete);
            this.Name = "DeletePatientForm";
            this.Text = "Usuwanie pacjenta";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxPatientNameDelete;
        private System.Windows.Forms.Label labelPatientNameDelete;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button buttonPatientDelete;
    }
}
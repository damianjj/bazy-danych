﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProjektBazyDanych
{
    public partial class ShowAllForm : Form
    {
        public ShowAllForm()
        {
            InitializeComponent();

            using (var ctx = new ClinicDbContext())
            {
                foreach (var q in ctx.Services)
                {
                    showRichTextBox.AppendText("Nazwa zabiegu: " + q.ServiceName.Name + "\nData: " + q.Duration + "\nNazwa pacjenta: " + q.Patient.Name + "\nNazwa pracownika: " + q.Employee.Name + "\n\n");
                }
            }
        }
    }
}

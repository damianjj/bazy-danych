﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProjektBazyDanych
{
    public partial class EmployeeForm : Form
    {
        public EmployeeForm(string who)
        {
            InitializeComponent();
            labelEmployeeForm.Text = "Zalogowano jako " + who + ".";
            Set(who);
        }

        string who1;

        private void Set(string who)
        {
            who1 = who;
        }

        private void ButtonAddPatient_Click(object sender, EventArgs e)
        {
            AddPatientForm f = new AddPatientForm(who1);
            f.Show();
        }

        private void ButtonDeletePatient_Click(object sender, EventArgs e)
        {
            DeletePatientForm f = new DeletePatientForm();
            f.Show();
        }

        private void ButtonSearchPatient_Click(object sender, EventArgs e)
        {
            FindPatientForm f = new FindPatientForm();
            f.Show();
        }

        private void FormClose(object sender, FormClosingEventArgs e)
        {
            MainForm f = new MainForm();
            f.Show();
        }

        private void ShowAllButton_Click(object sender, EventArgs e)
        {
            ShowAllForm f = new ShowAllForm();
            f.Show();
        }
    }
}

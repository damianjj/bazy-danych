﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProjektBazyDanych
{
    public partial class DeletePatientForm : Form
    {
        public DeletePatientForm()
        {
            InitializeComponent();
        }

        private int id;

        private void ButtonPatientDelete_Click(object sender, EventArgs e)
        {
            using (var ctx = new ClinicDbContext())
            {
                foreach (var q in ctx.Patients)
                {
                    if (q.Login == textBoxPatientNameDelete.Text)
                    {
                        id = q.PatientID;
                        ctx.Patients.Remove(q);
                    }
                }

                /*foreach (var q in ctx.Insurances)
                {
                    if (q.Patient.Login == textBoxPatientNameDelete.Text)
                    {
                        ctx.Insurances.Remove(q);
                    }
                }*/

                /*foreach (var q in ctx.Services)
                {
                    if (q.Patient.PatientID == id)
                    {
                        ctx.Services.Remove(q);
                    }
                }*/
                ctx.SaveChanges();
            }

            /*using (var ctx = new ClinicDbContext())
            {
                foreach (var q in ctx.Insurances)
                {
                    if (q.Patient.Login == textBoxPatientNameDelete.Text)
                    {
                        ctx.Insurances.Remove(q);
                    }
                }
                ctx.SaveChanges();
            }*/

            this.Close();
        }
    }
}

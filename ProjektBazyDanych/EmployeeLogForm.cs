﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProjektBazyDanych
{
    public partial class EmployeeLogForm : Form
    {
        public EmployeeLogForm()
        {
            InitializeComponent();
        }

        private void LogButton_Click(object sender, EventArgs e)
        {
            if (loginTextBox.Text == "" | passwdTextBox.Text == "")
            {
                MessageBox.Show("Wypełnij wszystkie pola!");
                return;
            }

            Employee log = new Employee();

            if (log.LogCheck(loginTextBox.Text, passwdTextBox.Text))
            {
                EmployeeForm f = new EmployeeForm(loginTextBox.Text);
                this.Hide();
                f.Show();
            }
            else MessageBox.Show("Błędny login lub hasło!");
        }

        private void FormClose(object sender, FormClosingEventArgs e)
        {
            MainForm f = new MainForm();
            f.Show();
        }

        private void BackLabel_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            this.Close();
        }
    }
}

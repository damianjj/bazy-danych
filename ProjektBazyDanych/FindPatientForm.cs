﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProjektBazyDanych
{
    public partial class FindPatientForm : Form
    {
        public FindPatientForm()
        {
            InitializeComponent();
        }

        int id;
        bool error = true;

        private void ButtonFindPatient_Click(object sender, EventArgs e)
        {
            using (var ctx = new ClinicDbContext())
            {
                foreach (var q in ctx.Patients)
                {
                    if (q.Login == textBoxPatientName.Text)
                    {
                        id = q.PatientID;
                        patientRichBox.AppendText("Imię i nazwisko: " + q.Name + "\nPESEL: " + q.PESEL + "\n");
                        error = false;
                    }
                }

                foreach (var q in ctx.Insurances)
                {
                    if (q.Patient.Login == textBoxPatientName.Text)
                    {
                        patientRichBox.AppendText("Nazwa ubezpieczyciela: " + q.CompanyName + "\nNumer ubezpieczenia: " + q.InsuranceNo + "\n");
                    }
                }

                foreach (var q in ctx.Services)
                {
                    if (q.Patient.PatientID == id)
                    {
                        patientRichBox.AppendText("Nazwa zabiegu: " + q.ServiceName.Name + "\nData zabiegu: " + q.Duration + "\nDo zapłaty: " + q.Cost + "\n");

                        foreach (var w in ctx.Employees)
                        {
                            if (q.Employee.EmployeeID == w.EmployeeID)
                            {
                                patientRichBox.AppendText("Pracownik: " + w.Name + "\n");
                            }
                        }
                    }
                }
            }

            if (error)
            {
                MessageBox.Show("Nie ma takiego pacjenta!");
            }
        }
    }
}

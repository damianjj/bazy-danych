﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjektBazyDanych
{
    public class Employee
    {
        public int EmployeeID { get; set; }
        public string Login { get; set; }
        public string Passwd { get; set; }
        public string Name { get; set; }

        public ICollection<Service> Services { get; set; }

        public Employee() { }

        public Employee(string login, string passwd, string name)
        {
            this.Login = login;
            this.Passwd = passwd;
            this.Name = name;
        }

        public bool LogCheck(string log, string passwd)
        {
            using (var ctx = new ClinicDbContext())
            {
                foreach (var q in ctx.Employees)
                {
                    if (log == q.Login & passwd == q.Passwd)
                    {
                        return true;
                    }
                }
            }
            return false;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProjektBazyDanych
{
    public partial class PatientForm : Form
    {
        public PatientForm(string who)
        {
            InitializeComponent();
            Set(who);
        }

        int id;

        private void Set(string who)
        {
            labelPatientForm.Text = "Zalogowano jako " + who + ".";

            using (var ctx = new ClinicDbContext())
            {
                foreach (var q in ctx.Patients)
                {
                    if (q.Login == who)
                    {
                        id = q.PatientID;
                        patientRichBox.AppendText("Imię i nazwisko: " + q.Name + "\nPESEL: " + q.PESEL + "\n");
                    }
                }

                foreach (var q in ctx.Insurances)
                {
                    if (q.Patient.Login == who)
                    {
                        patientRichBox.AppendText("Nazwa ubezpieczyciela: " + q.CompanyName + "\nNumer ubezpieczenia: " + q.InsuranceNo + "\n");
                    }
                }

                foreach (var q in ctx.Services)
                {
                    if (q.Patient.PatientID == id)
                    {
                        patientRichBox.AppendText("Nazwa zabiegu: " + q.ServiceName.Name + "\nData zabiegu: " + q.Duration + "\nDo zapłaty: " + q.Cost + "\n");

                        foreach (var w in ctx.Employees)
                        {
                            if (q.Employee.EmployeeID == w.EmployeeID)
                            {
                                patientRichBox.AppendText("Pracownik: " + w.Name + "\n");
                            }
                        }
                    }
                }
            }
        }

        private void FormClose(object sender, FormClosingEventArgs e)
        {
            MainForm f = new MainForm();
            f.Show();
        }
    }
}

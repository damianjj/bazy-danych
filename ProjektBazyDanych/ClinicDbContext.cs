﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjektBazyDanych
{
    public class ClinicDbContext: DbContext
    {
        public ClinicDbContext(): base(/* nazwa bazy */)
        {
            Database.SetInitializer(new CreateDatabaseIfNotExists<ClinicDbContext>());
        }

        public DbSet<Patient> Patients { get; set; }
        public DbSet<Service> Services { get; set; }
        public DbSet<Insurance> Insurances { get; set; }
        public DbSet<Employee> Employees { get; set; }
        public DbSet<ServiceName> ServiceNames { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProjektBazyDanych
{
    public partial class RegisterForm : Form
    {
        public RegisterForm()
        {
            InitializeComponent();
        }

        private void RegisterButtonClick(object sender, EventArgs e)
        {
            if (loginTextBox.Text == "" | passwdTextBox.Text == "" | nameTextBox.Text == "" | peselTextBox.Text == "" | insuranceNameTextBox.Text == "" | insuranceNoTextBox.Text == "")
            {
                MessageBox.Show("Wypełnij wszystkie pola!");
                return;
            }

            if (peselTextBox.TextLength != 9)
            {
                MessageBox.Show("Nieprawidłowy PESEL!");
                return;
            }

            try
            {
                Insurance i = new Insurance(insuranceNameTextBox.Text, int.Parse(insuranceNoTextBox.Text));
                Patient p = new Patient(loginTextBox.Text, passwdTextBox.Text, nameTextBox.Text, Int32.Parse(peselTextBox.Text), i);

                using (var ctx = new ClinicDbContext())
                {
                    ctx.Insurances.Add(i);
                    ctx.Patients.Add(p);
                    ctx.SaveChanges();
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show("Błąd!\nWprowadzono nieodpowiednie dane!");
            }
            

            MessageBox.Show("Pomyślnie zarejestrowano!\nTeraz możesz się zalogować!");
            this.Close();
        }

        private void FormClose(object sender, FormClosingEventArgs e)
        {
            MainForm f = new MainForm();
            f.Show();
        }
    }
}

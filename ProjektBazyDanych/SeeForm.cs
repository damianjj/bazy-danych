﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProjektBazyDanych
{
    public partial class SeeForm : Form
    {
        public SeeForm()
        {
            InitializeComponent();

            using (var ctx = new ClinicDbContext())
            {
                foreach (var q in ctx.ServiceNames)
                {
                    richTextBox1.AppendText("Nazwa zabiegu: " + q.Name + "\n\n");
                }
            }
        }

        private void FormClose(object sender, FormClosingEventArgs e)
        {
            MainForm f = new MainForm();
            f.Show();
        }
    }
}

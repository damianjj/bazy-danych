﻿namespace ProjektBazyDanych
{
    partial class FindPatientForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonFindPatient = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxPatientName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.patientRichBox = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // buttonFindPatient
            // 
            this.buttonFindPatient.Location = new System.Drawing.Point(94, 292);
            this.buttonFindPatient.Name = "buttonFindPatient";
            this.buttonFindPatient.Size = new System.Drawing.Size(130, 57);
            this.buttonFindPatient.TabIndex = 0;
            this.buttonFindPatient.Text = "Wyszukaj pacjenta";
            this.buttonFindPatient.UseVisualStyleBackColor = true;
            this.buttonFindPatient.Click += new System.EventHandler(this.ButtonFindPatient_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(91, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Dane pacjenta";
            // 
            // textBoxPatientName
            // 
            this.textBoxPatientName.Location = new System.Drawing.Point(94, 46);
            this.textBoxPatientName.Name = "textBoxPatientName";
            this.textBoxPatientName.Size = new System.Drawing.Size(155, 20);
            this.textBoxPatientName.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 49);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(77, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Login pacjenta";
            // 
            // patientRichBox
            // 
            this.patientRichBox.Enabled = false;
            this.patientRichBox.Location = new System.Drawing.Point(66, 72);
            this.patientRichBox.Name = "patientRichBox";
            this.patientRichBox.Size = new System.Drawing.Size(198, 193);
            this.patientRichBox.TabIndex = 6;
            this.patientRichBox.Text = "";
            // 
            // FindPatientForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(343, 372);
            this.Controls.Add(this.patientRichBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBoxPatientName);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.buttonFindPatient);
            this.Name = "FindPatientForm";
            this.Text = "Wyszukiwanie pacjenta";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonFindPatient;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxPatientName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RichTextBox patientRichBox;
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjektBazyDanych
{
    public class Insurance
    {
        [ForeignKey("Patient")]
        public int InsuranceID { get; set; }
        public string CompanyName { get; set; }
        public int InsuranceNo { get; set; }

        public virtual Patient Patient { get; set; }

        public Insurance() { }

        public Insurance(string companyName, int insuranceNo)
        {
            this.CompanyName = companyName;
            this.InsuranceNo = insuranceNo;
        }
    }
}

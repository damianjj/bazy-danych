﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjektBazyDanych
{
    public class Service
    {
        public int ServiceID { get; set; }
        //public string ServiceName { get; set; }
        public DateTime Duration { get; set; }
        public float Cost { get; set; }
        public virtual ServiceName ServiceName { get; set; }
        public virtual Patient Patient { get; set; }
        public virtual Employee Employee { get; set; }

        public Service() { }

        public Service(ServiceName serviceName, DateTime duration, float cost, Patient patient, Employee employee)
        {
            this.ServiceName = serviceName;
            this.Duration = duration;
            this.Cost = cost;
            this.Patient = patient;
            this.Employee = employee;
        }
    }
}
